from django.core.management.base import BaseCommand, CommandError
from mortalkombat.models import Fighter, Sport, Teams, ResultRound
from datetime import datetime
import pytz, requests, time
import sys
import getopt
import requests
import csv
import time
from bs4 import BeautifulSoup
from datetime import datetime
from multiprocessing import Pool
from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options


# Функция, которая забирает html код страницы по заданному url
def get_html(url):
    opts = Options()
    opts.set_headless()
    opts.add_argument('--headless')
    browser = Firefox(options=opts)
    browser.get(url)
    time.sleep(2)
    r = browser.page_source
    return r


# Функция, которая забирает html код страницы - 'https://www.sports.ru/tribuna/blogs/zerkalo1xbet/1448157.html'
# где находится ссылка на зеркало 1xbet, которое меняется каждый день
def get_site(html):
    soup = BeautifulSoup(html, 'lxml')
    div = soup.find('div', class_='material-item__content js-mediator-article').find('p').find('a').get('href')
    opts = Options()
    opts.set_headless()
    opts.add_argument('--headless')
    browser = Firefox(options=opts)
    browser.get(div)
    time.sleep(2)
    u = browser.current_url
    ur = u.find('/u')
    url = u[0:ur]
    urls = url + '/live/Mortal-Kombat/'
    return url, urls


# Функция, которая забирает ссылки всех игр
def get_all_links(html, url):
    soup = BeautifulSoup(html, 'lxml')
    divs = soup.find_all('a', class_='c-events__name')
    links = []
    for div in divs:
        a = div.get('href')
        link = str(url) + '/' + str(a)
        links.append(link)
    return links


# Функция, которая парсит коэффициенты игры
def get_koeff(html):
    soup = BeautifulSoup(html, 'lxml')
    count = 4
    names = []
    game = {}
    event = soup.find('div', class_='c-events main_game').get('id')
    game.update({'id': str(event)})
    player1 = soup.find_all('div', class_='nameCon')[0].find(text=True)
    player2 = soup.find_all('div', class_='nameCon')[1].find(text=True)
    game.update({'player1': str(player1)})
    game.update({'player2': str(player2)})
    f1 = soup.find_all('div', class_='bets betCols1')
    for k in f1:
        k = k.find_all('div', class_="")
        for j in k:
            j = j.find_all(text=True)
            titl = j[0]
            koeff = j[2]
            game.update({str(titl): str(koeff)})
    f2 = soup.find_all('div', class_='bets betCols2')
    for k in f2:
        k = k.find_all('div', class_="")
        for j in k:
            j = j.find_all(text=True)
            titl = j[0]
            koeff = j[2]
            game.update({str(titl): str(koeff)})
    f3 = soup.find_all('div', class_='blockSob')
    names = f3
    for name in names:
        batle = ''.join(name.findAll(text=True))
        titl = ''.join(name.find(text=True))
        n = batle.rfind(' ')
        koeff = batle[n:]
        game.update({str(titl): str(koeff)})
    return game


# Функция, которая меняет в json имена персонажей на (first and second)
def change_data(data, player1, player2):
    for key in data.keys():
        if key.find(str(player1)) != -1:
            old = key
            key = key.replace(str(player1), 'first')
            data[str(key)] = data.pop(str(old))
            print(key)
        elif key.find(str(player2)) != -1:
            old = key
            key = key.replace(str(player2), 'second')
            data[str(key)] = data.pop(str(old))
            print(key)
    return data


# Функция, которая добаваляет все коэффициенты в базу данных
def create_game(data, player1, player2, g_id):
    sport = Sport.objects.get(sport_id = 103)
    fighter1 = Fighter.objects.get(name_en = str(player1))
    fighter2 = Fighter.objects.get(name_en = str(player2))

    tmpTeams = Teams(fighter_first = fighter1, fighter_second = fighter2, sport = sport, game_id = g_id)
    if 'No Finish In 1 Round' in data:
        tmpTeams.net_victory_total = data['No Finish In 1 Round']
    if 'W first' in data:
        tmpTeams.first_1x2_win = data['W first']
    if 'W second' in data:
        tmpTeams.second_1x2_win = data['W second']
    if 'W first In 1 Round' in data:
        tmpTeams.first_win = data['W first In 1 Round']
    if 'W second In 1 Round' in data:
        tmpTeams.second_win = data['W second In 1 Round']
    if 'Total Over' in data:
        tmpTeams.total_over = data['Total Over']
    if 'Total Under' in data:
        tmpTeams.total_under = data['Total Under']
    if 'Total Over 5.5' in data:
        tmpTeams.total_over_55 = data['Total Over 5.5']
    if 'Total Under 5.5' in data:
        tmpTeams.total_under_55 = data['Total Under 5.5']
    if 'Total Over 6.5' in data:
        tmpTeams.total_over_65 = data['Total Over 6.5']
    if 'Total Under 6.5' in data:
        tmpTeams.total_under_65 = data['Total Under 6.5']
    if 'Total Over 8.5' in data:
        tmpTeams.total_over_85 = data['Total Over 8.5']
    if 'Total Under 8.5' in data:
        tmpTeams.total_under_85 = data['Total Under 8.5']
    if 'Fatality In 1 Round' in data:
        tmpTeams.fatality = data['Fatality In 1 Round']
    if 'Brutality In 1 Round' in data:
        tmpTeams.brutality = data['Brutality In 1 Round']
    if 'No Finish In 1 Round' in data:
        tmpTeams.without_finishing = data['No Finish In 1 Round']
    if 'Fatality In 1 Round - Yes' in data:
        tmpTeams.fatality_yes = data['Fatality In 1 Round - Yes']
    if 'Fatality In 1 Round - No' in data:
        tmpTeams.fatality_no = data['Fatality In 1 Round - No']
    if 'Individual Total first Over 2.5' in data:
        tmpTeams.individual_total_over_first = data['Individual Total first Over 2.5']
    if 'Individual Total first Under 2.5'in data:
        tmpTeams.individual_total_under_first = data['Individual Total first Under 2.5']
    if 'Individual Total second Over 2.5' in data:
        tmpTeams.individual_total_over_second = data['Individual Total second Over 2.5']
    if 'Individual Total second under 2.5' in data:
        tmpTeams.individual_total_under_second = data['Individual Total second under 2.5']
    if 'Individual Total first Over 1.5' in data:
        tmpTeams.individual_total_over_first_15 = data['Individual Total first Over 1.5']
    if 'Individual Total first Under 1.5'in data:
        tmpTeams.individual_total_under_first_15 = data['Individual Total first Under 1.5']
    if 'Individual Total second Over 1.5' in data:
        tmpTeams.individual_total_over_second_15 = data['Individual Total second Over 1.5']
    if 'Individual Total second under 1.5' in data:
        tmpTeams.individual_total_under_second_15 = data['Individual Total second under 1.5']
    if 'Individual Total first Over 3.5' in data:
        tmpTeams.individual_total_over_first_35 = data['Individual Total first Over 3.5']
    if 'Individual Total first Under 3.5' in data:
        tmpTeams.individual_total_under_first_35 = data['Individual Total first Under 3.5']
    if 'Individual Total second Over 3.5' in data:
        tmpTeams.individual_total_over_second_35 = data['Individual Total second Over 3.5']
    if 'Individual Total second under 3.5' in data:
        tmpTeams.individual_total_under_second_35 = data['Individual Total second under 3.5']
    if 'Total Fatalities Over 0.5' in data:
        tmpTeams.fatality_total_over_05 = data['Total Fatalities Over 0.5']
    if 'Total Fatalities Under 0.5' in data:
        tmpTeams.fatality_total_under_05 = data['Total Fatalities Under 0.5']
    if 'Total Brutalities Over 0.5' in data:
        tmpTeams.brutality_total_over_05 = data['Total Brutalities Over 0.5']
    if 'Total Brutalities Under 0.5' in data:
        tmpTeams.brutality_total_under_05 = data['Total Brutalities Under 0.5']
    if 'Total Fatalities Over 2.5' in data:
        tmpTeams.fatality_total_over_25 = data['Total Fatalities Over 2.5']
    if 'Total Fatalities Under 2.5' in data:
        tmpTeams.fatality_total_under_25 = data['Total Fatalities Under 2.5']
    if 'Total Brutalities Over 2.5' in data:
        tmpTeams.brutality_total_over_25 = data['Total Brutalities Over 2.5']
    if 'Total Brutalities Under 2.5' in data:
        tmpTeams.brutality_total_under_25 = data['Total Brutalities Under 2.5']
    if 'Total Fatalities Over 4.5' in data:
        tmpTeams.fatality_total_over_45 = data['Total Fatalities Over 4.5']
    if 'Total Fatalities Under 4.5' in data:
        tmpTeams.fatality_total_under_45 = data['Total Fatalities Under 4.5']
    if 'Total Brutalities Over 4.5' in data:
        tmpTeams.brutality_total_over_45 = data['Total Brutalities Over 4.5']
    if 'Total Brutalities Under 4.5' in data:
        tmpTeams.brutality_total_under_45 = data['Total Brutalities Under 4.5']
    tmpTeams.save()


# Основная функция
def MkParser():
    url = 'https://www.sports.ru/tribuna/blogs/zerkalo1xbet/1448157.html'
    m = get_html(url)
    url, urls = get_site(m)
    r = get_html(str(urls))
    all_links = get_all_links(r, url)
    num_new_game = len(all_links) - 1
    html = get_html(str(all_links[num_new_game]))
    data = get_koeff(html)
    player1 = data['player1']
    player2 = data['player2']
    player1 = player1.strip()
    player2 = player2.strip()
    g_id = data['id']
    info = change_data(data, player1, player2)
    create_game(info, player1, player2, g_id)

    '''
    if in data:
        tmpTeams.result_brutality = data[]
    if in data:
        tmpTeams.result_fatality = data[]
    tmpTeams.result = data[]
    tmpTeams.result_round_1 = data[]
    tmpTeams.result_round_2 = data[]
    tmpTeams.result_round_3 = data[]
    tmpTeams.result_round_4 = data[]
    tmpTeams.result_round_5 = data[]
    tmpTeams.result_round_6 = data[]
    tmpTeams.result_round_7 = data[]
    tmpTeams.result_round_8 = data[]
    tmpTeams.result_round_9 = data[]
    
    if in data:
        tmpTeams.round_time = data[]
    if in data:
        tmpTeams.round_time_over = data[]
    if in data:
        tmpTeams.round_time_under = data[]
    if in data:
        tmpTeams.round_time_min = data[]
    if in data:
        tmpTeams.round_time_min_over = data[]
    if in data:
        tmpTeams.round_time_min_under = data[]
    if in data:
        tmpTeams.round_time_max = data[]
    if in data:
        tmpTeams.round_time_max_over = data[]
    if in data:
        tmpTeams.round_time_max_under = data[]
   '''


# Пример словаря:
'''
{'Individual Total second under 2.5': '3.26', 'Fatality In 1 Round': '5.15', 'Individual Total first Over 3.5': '1.34', 'Total Over 6.5': '1.23', 
'Individual Total first Under 3.5': '3.38', 'Individual Total first Over 1.5': '1.03', 'Total Fatalities Over 2.5': '6.7', 'Total Over 8.5': '3.58', 
'Total Under 5.5': '14', 'Total Brutalities Over 2.5': '2.048', 'Total Brutalities Under 4.5': '1.025', 'Total Brutalities Under 2.5': '1.86', 
'Total Under 7.5': '2.096', 'Brutality In 1 Round': '2.82', 'Fatality In 1 Round - Yes': '5.25', 'W Leatherface': '2.39', 'W second In 1 Round': '2.1', 
'Individual Total first Over 2.5': '1.14', 'Total Over 5.5': '1.016', 'Individual Total first Under 1.5': '12', 'Individual Total second under 3.5': '2.152', 
'Individual Total first Under 2.5': '5.6', 'Total Fatalities Under 0.5': '4.06', 'Individual Total second under 1.5': '6.3', 'id': '172956044', 
'W first': '1.632', 'player2': 'Leatherface ', 'Individual Total second Over 2.5': '1.36', 'player1': 'Quan Chi', 'Individual Total second Over 1.5': '1.115', 
'W first In 1 Round': '1.8', 'Total Fatalities Over 0.5': '1.24', 'Individual Total second Over 3.5': '1.765', 'Total Under 6.5': '4.18', 
'Total Brutalities Over 4.5': '12', 'Fatality In 1 Round - No': '1.155', 'No Finish In 1 Round': '1.96', 'Total Fatalities Under 2.5': '1.104', 
'Total Under 8.5': '1.31', 'Total Brutalities Under 0.5': '17', 'Total Over 7.5': '1.805'}

'''


class Command(BaseCommand):
    help = 'Start parsing process'

    def handle(self, *args, **options):
        while True:
            MkParser()

