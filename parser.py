import sys
import getopt
import requests
import csv
import time
from bs4 import BeautifulSoup
from datetime import datetime
from multiprocessing import Pool
from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
 
def get_html(url):
  opts = Options()
  opts.set_headless()
  opts.add_argument('--headless') 
  browser = Firefox(options=opts)
  browser.get(url)
  time.sleep(2)
  r = browser.page_source
  return r
  
def get_site(html):
	soup = BeautifulSoup(html, 'lxml')
	div = soup.find('div', class_ = 'material-item__content js-mediator-article').find('p').find('a').get('href')
	opts = Options()
	opts.set_headless()
	opts.add_argument('--headless')
	browser = Firefox(options=opts)
	browser.get(div)
	time.sleep(2)
	u = browser.current_url
	ur = u.find('/u')
	url = u[0:ur]
	urls = url + '/live/Mortal-Kombat/'
	return url, urls

def get_all_links(html, url):
   soup = BeautifulSoup(html, 'lxml')
   divs = soup.find_all('a', class_ = 'c-events__name')
   links = []
   for div in divs:
     a = div.get('href')
     link = str(url) + '/' + str(a)
     links.append(link)
   return links

def get_koeff(html):
	soup = BeautifulSoup(html, 'lxml')
	count = 4
	names = []
	game = {}
	event = soup.find('div', class_='c-events main_game').get('id')
	game.update({'id':str(event)})
	player1 = soup.find_all('div', class_='nameCon')[0].find(text = True)
	player2 = soup.find_all('div', class_='nameCon')[1].find(text = True)
	game.update({'player1':str(player1)})
	game.update({'player2':str(player2)})
	f1 = soup.find_all('div', class_ = 'bets betCols1')
	for k in f1:
		k = k.find_all('div', class_= "")
		for j in k:
			j = j.find_all(text = True)
			titl = j[0]
			koeff = j[2]
			game.update({str(titl): str(koeff)})
	f2 = soup.find_all('div', class_ = 'bets betCols2')
	for k in f2:
		k = k.find_all('div', class_= "")
		for j in k:
			j = j.find_all(text = True)
			titl = j[0]
			koeff = j[2]
			game.update({str(titl): str(koeff)})
	f3 = soup.find_all('div', class_ = 'blockSob')
	names = f3
	for name in names:
	  batle = ''.join(name.findAll(text = True))
	  titl = ''.join(name.find(text = True))
	  n = batle.rfind(' ')
	  koeff = batle[n:]
	  game.update({str(titl): str(koeff)})
	return game
	
def change_data(data):
	player1 = data['player1']
	player2 = data['player2']
	player1 = player1.strip()
	player2 = player2.strip()
	p1 = player1 + '///'
	p2 = player2 + '///'
	print(p1)
	print(p2)
	for key in data.keys():
		if key.find(str(player1)) != -1:
			old = key
			key = key.replace(str(player1), 'first')
			data[str(key)] = data.pop(str(old))
			print(key)
		elif key.find(str(player2)) != -1:
			old = key
			key = key.replace(str(player2), 'second')
			data[str(key)] = data.pop(str(old))
			print(key)
	return data			
			

def main():
  url = 'https://www.sports.ru/tribuna/blogs/zerkalo1xbet/1448157.html'
  m = get_html(url)
  url, urls = get_site(m)
  r = get_html(str(urls))
  all_links = get_all_links(r, url)
  num_new_game = len(all_links) - 1
  html = get_html(str(all_links[num_new_game]))
  data = get_koeff(html)
  info = change_data(data)
  
  #g_id = data['id']
  #add_koeff(g_id)
  #print(str(data))
  print(str(info))
  
  #for urls in all_links:
	  #html = get_html(str(urls))
	  #data = get_koeff(html)
	  #print(str(data))

	 # map(function, list_)
	 # with Pool(40) as p:
		#  p.map(make_all, all_links)

if __name__ == "__main__":
  sys.exit(main())
